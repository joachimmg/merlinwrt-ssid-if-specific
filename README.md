# AIOP - Assign IP to OpenVPN for PIA
(Private Internett Access - https://privateinternettaccess.com) 

## Merlin-WRT/Asuswrt-merlin SSID InterFace Specific

This code is used to assign a new DHCP range to an interface and route the traffic over an OpenVPN for PIA (may work with others). 

Code is based on
* [How to setup SSID for VPN and SSID for Regular ISP using OpenVPN.](https://github.com/RMerl/asuswrt-merlin/wiki/How-to-setup-SSID-for-VPN-and-SSID-for-Regular-ISP-using-OpenVPN.)
* [How to have dedicated DHCP options bind to a specific SSID?](https://github.com/RMerl/asuswrt-merlin/wiki/How-to-have-dedicated-DHCP-options-bind-to-a-specific-SSID%3F)
but is developed with the mind of easy configurationi. Code is separated from configuration files.

The main reason for doing this was that I wanted several Wi-Fi networks with traffic routed over different kind of VPN-connections. See screenshots below.

## Quick how to
1. Enable the guest network(s) you want to use, and make the configuration needed/wanted (key etc) in the WebUI.
2. Clone this repo locally and do the following edit:
    1. Open the file dnsmasq.postconf and fill inn the networks you want to route through a VPN, in the format `assign_ip "<interface>" "<dhcp>" "<range from>" "<range to>"` e.g `assign_ip "wl0.1" "192.168.2.1" "2" "254"` will start a DHCP server on IP 192.168.2.1 with a range from 192.168.2.2 - 192.168.2.254.
    2. Open the file openvpn-event and fill in the network interfaces you want to route, in the format `route_vpn "<interface g.network>" "<interface vpn>"`, e.g `route_vpn "wl0.1" "tun11"` to route wl0.1 over tun11.
3. Log on to your router and create a directory named "aiop" in the directory `/jffs/scripts`.
3. Push the files from your local machine to the JFFS-partition on your router: 
    1. `scp /path/to/aiop/* <username>@ip-address:/jffs/scripts/aiop`
    2. `scp dnsmasq.postconf <username>@ip-address:/jffs/scripts/`
    3. `scp openvpn-event <username>@ip-address:/jffs/scripts/`
4. Restart the router.

You will find an example on how I've done it, with 6 guest networks and 5 VPN-clients, in the files "dnsmasq.postconf" and "openvpn-event". Note that wl0.1 and wl0.3 shares VPN-client. This way you can create several VPN-clients, i.e with different locations, and connect to the respective Wi-Fi when you want to change VPN-connetion.

You will also find an configuration file in the "client" directory, that I have exported from my setup, that you can import in the WebUI. I have included the two certificates you need to copy/paste into the WebUI certificate-forms in the directory as well.

# Screenshots
![ESSID list](https://git.giaever.org/joachimmg/merlinwrt-ssid-if-specific/raw/master/screenshots/Screenshot%20from%202017-02-07%2021-57-38.png "List over SSIDs with a describing name of what country they 'belong to'")
![ESSID list](https://git.giaever.org/joachimmg/merlinwrt-ssid-if-specific/raw/master/screenshots/Screenshot%20from%202017-02-07%2021-49-40.png "List over SSIDs with a describing name of what country they 'belong to'")
![Connection](https://git.giaever.org/joachimmg/merlinwrt-ssid-if-specific/raw/master/screenshots/Screenshot%20from%202017-02-07%2021-51-16.png "Connected to a server in Australia")
